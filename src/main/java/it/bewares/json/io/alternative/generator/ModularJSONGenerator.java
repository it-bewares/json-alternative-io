/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.alternative.generator;

import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.generator.JSONGenerator;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

/**
 * This JSON generator is meant to be a modular generator without any
 * extensions. However with the an easy method to extend by overriding existing
 * methods to e.g. add whitespaces.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
@Singleton
public class ModularJSONGenerator implements JSONGenerator {

    protected void separateItems(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.SEPARATOR);
    }

    protected void separateKey(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.KEY_VALUE_SEPARATOR);
    }

    protected void beginObject(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.OBJECT_START);
    }

    protected void endObject(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.OBJECT_END);
    }

    protected void beginArray(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.ARRAY_START);
    }

    protected void endArray(Writer writer) throws JSONFormatException, IOException {
        writer.append(JSONFactory.ARRAY_END);
    }

    protected void object(JSONObject json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("object() generating json object text");

        this.beginObject(writer);

        Iterator<Map.Entry<String, JSONValue>> iterator = json.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();

            JSONString key = new JSONString((String) entry.getKey());
            JSONValue value = (JSONValue) entry.getValue();

            this.delegate(key, writer);

            this.separateKey(writer);

            this.delegate(value, writer);

            if (iterator.hasNext()) {
                this.separateItems(writer);
            }
        }

        this.endObject(writer);
    }

    protected void array(JSONArray json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("array() generating json array text");

        this.beginArray(writer);

        Iterator<JSONValue> iterator = json.iterator();
        while (iterator.hasNext()) {
            JSONValue element = iterator.next();

            this.delegate(element, writer);

            if (iterator.hasNext()) {
                this.separateItems(writer);
            }
        }

        this.endArray(writer);
    }

    protected void string(JSONString json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("string() generating json string text");

        writer.append(JSONFactory.STRING_SURROUND);

        for (int i = 0; i < json.length(); i++) {
            char c = json.charAt(i);

            switch (c) {
                case JSONFactory.STRING_SURROUND:
                case '\\':
                case '/':
                    writer.append(JSONFactory.STRING_ESCAPE).append(c);
                    break;

                case '\b':
                    writer.append("\\b");
                    break;

                case '\f':
                    writer.append("\\f");
                    break;

                case '\n':
                    writer.append("\\n");
                    break;

                case '\r':
                    writer.append("\\r");
                    break;

                case '\t':
                    writer.append("\\t");
                    break;

                default:
                    if (c <= '\u001F') {
                        String escaped = Integer.toHexString((int) c);
                        writer.append("\\u000".substring(0, 6 - escaped.length())).append(escaped);
                    } else {
                        writer.append(c);
                    }
                    break;
            }
        }

        writer.append(JSONFactory.STRING_SURROUND);
    }

    protected void number(JSONNumber json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("number() generating json number text");

        writer.append(json.toString());
    }

    protected void bool(JSONBoolean json, Writer writer) throws IOException {
        LOG.debug("bool() generating json boolean text");

        writer.append(json.toString());
    }

    protected void nul(JSONValue json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("nul() generating json null text");

        if (json == null) {
            writer.append("null");
        } else {
            throw new JSONFormatException();

        }
    }

    protected void delegate(JSONValue json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("delegate() delegating json text generation");

        JSONFactory.Type type;
        if (json == null) {
            type = JSONFactory.Type.NULL;
        } else {
            type = json.type();
        }

        try {
            switch (type) {
                case OBJECT:
                    this.object((JSONObject) json, writer);
                    break;

                case ARRAY:
                    this.array((JSONArray) json, writer);
                    break;

                case STRING:
                    this.string((JSONString) json, writer);
                    break;

                case NUMBER:
                    this.number((JSONNumber) json, writer);
                    break;

                case BOOLEAN:
                    this.bool((JSONBoolean) json, writer);
                    break;

                case NULL:
                    this.nul(json, writer);
                    break;

                default:
                    throw new JSONFormatException();
            }
        } catch (ClassCastException ex) {
            throw new JSONFormatException(ex);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(JSONValue json, Writer writer) throws JSONFormatException, IOException {
        this.delegate(json, writer);
    }

}
