/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.alternative.parser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONValue;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONObject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.parser.JSONParser;

/**
 * This JSON parse is meant to be a minimalistic parser without any extensions
 * or optimizations for spicific parsing situations.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
@Singleton
public class SimpleJSONParser implements JSONParser {

    private static final String NUMBER_REGEXP = "^(-?(?:0|[1-9]\\d*)(?:\\.\\d+)?(?:[eE](?:\\+|\\-)?\\d+)?)";
    private static final String STRING_REGEXP = "^(\\\"(?:[^\\\"\\\\\u0000-\u001F]|\\\\(?:[\\\"\\\\\\/bfnrt]|u[\\dA-Fa-f]{4}))*\\\")";
    private static final Set<Character> WHITESPACE_CHARACTERS = new HashSet<>(Arrays.asList(' ', '\t', '\n', '\r'));

    private static void throwInvalid(String input, Throwable cause) throws JSONFormatException {
        throw new JSONFormatException("input" + input + " is not valid", cause);
    }

    private static String firstChunk(String input) throws JSONFormatException {
        if (input.length() == 0) {
            SimpleJSONParser.throwInvalid(input, null);
        }

        char first = input.charAt(0);

        int to = 0;

        switch (first) {
            case JSONFactory.ARRAY_START:
                int arrayIndex = 1;

                while (arrayIndex != 0) {
                    char next = 0;
                    try {
                        next = input.charAt(++to);
                    } catch (StringIndexOutOfBoundsException ex) {
                        SimpleJSONParser.throwInvalid(input, ex);
                    }

                    switch (next) {
                        case JSONFactory.ARRAY_START:
                            arrayIndex++;
                            break;

                        case JSONFactory.ARRAY_END:
                            arrayIndex--;
                            break;

                        case JSONFactory.STRING_SURROUND:
                            to += SimpleJSONParser.firstChunk(input.substring(to)).length() - 1;
                            break;
                    }
                }

                to++;
                break;

            case JSONFactory.OBJECT_START:
                int objectIndex = 1;

                while (objectIndex != 0) {
                    char next = 0;
                    try {
                        next = input.charAt(++to);
                    } catch (StringIndexOutOfBoundsException ex) {
                        SimpleJSONParser.throwInvalid(input, ex);
                    }

                    switch (next) {
                        case JSONFactory.OBJECT_START:
                            objectIndex++;
                            break;

                        case JSONFactory.OBJECT_END:
                            objectIndex--;
                            break;

                        case JSONFactory.STRING_SURROUND:
                            to += SimpleJSONParser.firstChunk(input.substring(to)).length() - 1;
                            break;
                    }
                }

                to++;
                break;

            case JSONFactory.STRING_SURROUND:
                Matcher stringPatternMatcher = Pattern.compile(SimpleJSONParser.STRING_REGEXP).matcher(input);
                if (stringPatternMatcher.find()) {
                    to = stringPatternMatcher.end();
                }
                break;

            default:
                Matcher numberPatternMatcher = Pattern.compile(SimpleJSONParser.NUMBER_REGEXP).matcher(input);
                if (numberPatternMatcher.find()) {
                    to = numberPatternMatcher.end();
                } else if (input.startsWith("true") || input.startsWith("null")) {
                    to = 4;
                } else if (input.startsWith("false")) {
                    to = 5;
                }
                break;
        }

        if (to == 0) {
            SimpleJSONParser.throwInvalid(input, null);
        }

        return input.substring(0, to);
    }

    private void validate(String input) {
        String inputImportantChars = SimpleJSONParser.removeUnimportantCharacters(input);

        LOG.debug("validate() validation {}", inputImportantChars);

        if (inputImportantChars.length() != SimpleJSONParser.firstChunk(inputImportantChars).length()) {
            SimpleJSONParser.throwInvalid(input, null);
        }
    }

    private static String removeUnimportantCharacters(String input) {
        StringBuilder sb = new StringBuilder();

        int index = -1;

        boolean escaped = false;
        boolean inString = false;
        boolean inLiteral = false;

        while (++index < input.length()) {
            char c = input.charAt(index);

            if (c == JSONFactory.STRING_SURROUND && !escaped) {
                inString = !inString;

                if (inLiteral) {
                    SimpleJSONParser.throwInvalid(input, null);
                }
            }

            if (Arrays.asList('t', 'f', 'n').contains(c) && !escaped && !inString) {
                inLiteral = true;
            }

            if (Arrays.asList('e', 'l').contains(c) && inLiteral) {
                inLiteral = false;
            }

            if (inLiteral || inString || !SimpleJSONParser.WHITESPACE_CHARACTERS.contains(c)) {
                sb.append(c);
            }

            escaped = c == JSONFactory.STRING_ESCAPE;
        }

        return sb.toString();
    }

    private static JSONFactory.Type getType(String input) {
        switch (input.charAt(0)) {
            case JSONFactory.OBJECT_START:
                return JSONFactory.Type.OBJECT;

            case JSONFactory.ARRAY_START:
                return JSONFactory.Type.ARRAY;

            case JSONFactory.STRING_SURROUND:
                return JSONFactory.Type.STRING;

            default:
                if (input.matches(SimpleJSONParser.NUMBER_REGEXP)) {
                    return JSONFactory.Type.NUMBER;
                }

                if ("true".equals(input) || "false".equals(input)) {
                    return JSONFactory.Type.BOOLEAN;
                }

                if ("null".equals(input)) {
                    return JSONFactory.Type.NULL;
                }
        }

        SimpleJSONParser.throwInvalid(input, null);
        return null;
    }

    private JSONValue parseSubSequence(String input) {
        LOG.debug("parseSubSequence() parsing json subsequence");

        String inputImportantChars = SimpleJSONParser.removeUnimportantCharacters(input);
        JSONFactory.Type type = SimpleJSONParser.getType(inputImportantChars);
        JSONValue json = null;

        try {
            json = (JSONValue) type.createInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            SimpleJSONParser.throwInvalid(input, null);
        }

        String remainingInput;

        switch (type) {
            case OBJECT:
                remainingInput = inputImportantChars.substring(1, inputImportantChars.length() - 1);

                while (remainingInput.length() > 0) {
                    String key = SimpleJSONParser.firstChunk(remainingInput);

                    remainingInput = remainingInput.substring(key.length());
                    try {
                        if (JSONFactory.KEY_VALUE_SEPARATOR == remainingInput.charAt(0)) {
                            remainingInput = remainingInput.substring(1);
                        } else {
                            SimpleJSONParser.throwInvalid(input, null);
                        }
                    } catch (StringIndexOutOfBoundsException ex) {
                        SimpleJSONParser.throwInvalid(input, ex);
                    }

                    String value = SimpleJSONParser.firstChunk(remainingInput);

                    remainingInput = remainingInput.substring(value.length());
                    if (remainingInput.length() > 0) {
                        if (JSONFactory.SEPARATOR == remainingInput.charAt(0)) {
                            remainingInput = remainingInput.substring(1);
                        } else {
                            SimpleJSONParser.throwInvalid(input, null);
                        }
                    }

                    ((JSONObject<JSONValue>) json).put(JSONString.unescape(key), this.parseSubSequence(value));

                    LOG.debug("parse() putting {} -> {} in json object", JSONString.unescape(key), value.length() > 50 ? value.substring(0, 50) : value);
                }
                break;

            case ARRAY:
                remainingInput = inputImportantChars.substring(1, inputImportantChars.length() - 1);

                while (remainingInput.length() > 0) {
                    String element = SimpleJSONParser.firstChunk(remainingInput);

                    ((JSONArray<JSONValue>) json).add(this.parseSubSequence(element));

                    LOG.debug("parse() putting {} in json array", element.length() > 50 ? element.substring(0, 50) : element);

                    remainingInput = remainingInput.substring(element.length());

                    if (remainingInput.length() > 0) {
                        if (JSONFactory.SEPARATOR == remainingInput.charAt(0)) {
                            remainingInput = remainingInput.substring(1);
                        } else {
                            SimpleJSONParser.throwInvalid(input, null);
                        }
                    }
                }
                break;

            case STRING:
                LOG.debug("parse() filling json string");
                ((JSONString) json).setValue(JSONString.unescape(input));
                break;

            case NUMBER:
                LOG.debug("parse() filling json number");
                ((JSONNumber) json).parseAndSetValue(input);
                break;

            case BOOLEAN:
                LOG.debug("parse() filling json boolean");
                ((JSONBoolean) json).setValue("true".equals(input));
                break;

            case NULL:
                LOG.debug("parse() having json null");
                // null is null => do nothing
                break;
        }

        return json;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONValue parse(String input) throws JSONFormatException {
        LOG.debug("parse() parsing json");
        this.validate(input);
        return this.parseSubSequence(input);
    }

}
