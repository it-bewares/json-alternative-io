/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.alternative.test.generator;

import it.bewares.json.io.generator.JSONGenerator;
import it.bewares.json.io.alternative.generator.ModularJSONGenerator;
//import it.bewares.json.test.io.generator.GenerationTest;
import java.io.IOException;
import java.io.Writer;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

/**
 * This test class is to test the alternative implemention of JSONGenerator
 * delivered with this project.
 *
 * @author Markus Goellnitz
 */
public class AlternativeGenerationTest /*extends GenerationTest*/ {

    @Factory(dataProvider = "generators")
    public AlternativeGenerationTest(JSONGenerator generator, String itemSeparator) {
        //super(generator, itemSeparator);
    }

    @DataProvider
    public static Object[][] generators() {
        return new Object[][]{
            {new ModularJSONGenerator(), ","},
            {new ModularJSONGenerator() {

                @Override
                protected void separateItems(Writer writer) throws IOException {
                    writer.append(", ");
                }
            }, ", "}
        };
    }

}
